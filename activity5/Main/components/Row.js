import React, { Component } from 'react'
import { TouchableOpacity, Text, StyleSheet } from 'react-native'


class Row extends Component {

    handleOperation = () => {
    this.props.operation(this.props.char)
    }

    componentDidMount = () => {
    }

    render() {
    return (
    <TouchableOpacity
        onPress={() => this.props.operation(this.props.char)}
        style={styles.container}
    >
        <Text style={styles.text}>{this.props.char}</Text>
    </TouchableOpacity>
    )
    }
}

const styles = StyleSheet.create({
    container:{
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    },
    text: {
    color: '#000000',
    fontWeight: 'bold',
    fontSize: 30

    }
})

export default Row;
