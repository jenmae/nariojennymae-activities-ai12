import React, { Component } from 'react'
import { View, SafeAreaView, Text, StyleSheet } from 'react-native'

const Design = ({ state }) => (
<View style={styles.container}>
    <SafeAreaView style={styles.safe}>
    <Text
        style={styles.display}
        adjustsFontSizeToFit
        numberOfLines={1}
    >{state.display}</Text>
    { state.result !== '' &&
        <Text
        style={styles.result}
        adjustsFontSizeToFit
        numberOfLines={1}
        >{state.result}</Text>
    }
    </SafeAreaView>
</View>
)

const styles = StyleSheet.create({
container: {
    backgroundColor: '#c0c0c0',
    flex: 1,
    justifyContent: 'space-around',
    paddingHorizontal: 24,
},
safe: {
    flex: 1,
    justifyContent: 'space-around',
},
display: {
    textAlign: 'right',
    fontWeight: 'normal',
    color: '#000000',
    fontSize: 40,
},
result: {
    textAlign: 'right',
    fontWeight: 'bold',
    color: '#808080',
    fontSize: 30,
    
},
})

export default Design;