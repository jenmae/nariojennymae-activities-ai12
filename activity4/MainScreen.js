import React from "react";
import { Image, Text, View } from "react-native";
import AppIntroSlider from "react-native-app-intro-slider";

//contents of the slide
const slides = [
    {
    key: "first",
    title: "WHY I CHOSE IT COURSE?", 
    text: 
    "Let's Go and Find Out!",
    image: require("./image/homescreen.png"),
    },
    {
    key: "second",
    title: "Low cost of Education ",
    text:
    "You don't need a 4-year degree to become an I.T. professional.",
    image: require("./image/slide1.png"), 
    },
    {
    key: "third",
    title: "Helps in upgrading your Skills",
    text:
    "Getting this degree will help in getting a good job with a good pay package.",
    image: require("./image/slide2.png"),
    },
    {
    key: "fourth",
    title: "Find Work Almost Anywhere",
    text:
    "You can find work in just about every industry you’d like, such as financial services, entertainment, government, hospitality and more.",
    image: require("./image/slide3.png"),
    },
    {
    key: "fifth",
    title: "Explore Your Creative Side",
    text:
    "You have the opportunity to use your skills to make major contributions.",
    image: require("./image/slide4.png"),
    },
    {
    key: "sixth",
    title: "Turn Your Talent into a Career",
    text:
    "You’re in a unique and wonderful position to turn something that’s always been a hobby into a rewarding career.",
    image: require("./image/slide5.png"),
    },
]

const Slide = ({ item }) => {
    return (
    <View style={{ flex: 1, backgroundColor: '#f08080'}}>
        <Image source={item.image}
        style={{
            resizeMode: 'center',
            height: "60%",
            width: "100%",
        }}
        />
        <Text style={{
            paddingTop: 0,
            paddingBottom: 10,
            fontSize: 30,
            fontWeight: "bold",
            color: "#800080",
            alignSelf: "center",
        }}
        >
        {item.title}
        </Text>

        <Text style={{
        textAlign:"center",
        color:"#000000",
        fontSize:18,
        paddingHorizontal:20,
        }}>
        {item.text}
        </Text>
    </View>
    )
    };

    export default function IntroSliders({navigation}) {
    return(
        <AppIntroSlider
        renderItem={({item}) => <Slide item={item}/> } 
        data={slides} 
        activeDotStyle={{
            backgroundColor:"#000000",
            width:20
        }}
        onDone={() => navigation.push('Intro')}
        showDoneButton={true}
        renderDoneButton={()=> <Text style={{color: '#800000', fontWeight: 'bold', margin: 5, fontSize: 20}}>
            Home</Text>}
        showNextButton={true}
        renderNextButton={()=> <Text style={{color: '#0000cd', fontWeight: 'bold', margin: 5, fontSize: 20}}>
            Continue</Text>}
        /> 
    ) 
};