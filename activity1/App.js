import { StatusBar } from 'expo-status-bar';
import { StyleSheet, Text, View } from 'react-native';

export default function App() {
  return (
    <View style={styles.container}>
      <input id = "input-test" type= "text"></input>
      <button onClick={mainFunction}>Submit</button>
    </View>
  );
}

function mainFunction() {
  alert(document.getElementById("input-test").value);
}
const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',
    alignItems: 'center',
    justifyContent: 'center',
  },
});
