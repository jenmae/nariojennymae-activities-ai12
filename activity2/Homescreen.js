import { View, Text } from "react-native";
import React from "react";

const Homescreen = () => {
    
    return (
    <View

    style={{
        height: "80%",
        backgroundColor: "white",
        justifyContent: "center",
        alignItems: "center",

    }}
    > <Text style={{ fontSize: 20, fontWeight: "bold", letterSpacing: 5 ,color: '#3E2723'}}>
        Jenny mae A. Nario. </Text>
    </View>
    );
};

export default Homescreen;