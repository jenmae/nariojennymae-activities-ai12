import React, { useState } from "react";
import { StyleSheet, Text, View, TextInput, KeyboardAvoidingView, TouchableOpacity,} from "react-native";
import { color } from " react-native/Libraries/Components/View/ReactNativeStyleAttributes";
import Task from "../components/Task";

export default function App() {
    const [task, setTask] = useState();
    const [taskItems, setTaskItems] = useState([]);
    
    const handleAddTask = () => {
    setTaskItems([...taskItems, task]);
    setTask(null);
    
    };

    const comepleteTask = (index) => {
    let itemsCopy = [...taskItems];
    itemsCopy.splice(index, 1);
    setTaskItems(itemsCopy);
    
    };

    return (
    <View style={styles.container}>  
    <View style={styles.taskWrapper}>
        <Text style={styles.sectionTitle}>My To Do Task</Text>
        <View style={styles.items}> {taskItems.map((item, index) => {
            return (  
            <TouchableOpacity
                key={index}
                onPress={() => comepleteTask(index)}
            >
                <Task text={item} /> </TouchableOpacity>
            );
            
            })}
        </View> 
        </View>
        
        <KeyboardAvoidingView
    
        behavior={Platform.OS === "android" ? "padding" : "height"}
        style={styles.writeTaskWrapper} >
    
        <TextInput style={styles.input} placeholder={"Write your Task"} value={task} onChangeText={(text) => setTask(text)}
        />
        <TouchableOpacity onPress={() => handleAddTask()}>
        <View style={styles.addWrapper}>
            <Text style={styles.addText}>+</Text> </View>
        </TouchableOpacity> </KeyboardAvoidingView>
    </View>
    );
}

const styles = StyleSheet.create({
    
    container: {
    flex: 1,
    backgroundColor: "#fff8dc",
    },
    sectionTitle: {
    fontSize: 25,
    fontWeight: "bold",
    color: "#3E2723",
    },
    
    taskWrapper: {
    paddingTop: 80,
    paddingHorizontal: 20,
    },

    
    items: {
    marginTop: 30,
    },

    writeTaskWrapper: {
    position: "absolute",
    bottom: 60,
    width: "100%",
    flexDirection: "row",
    justifyContent: "space-between",
    alignItems: "center",
    },

    input: {
    paddingVertical: 15,
    paddingHorizontal: 15,
    backgroundColor: "#A1887F",
    borderRadius: 60,
    borderColor: "#3E2723",
    borderWidth: 1,
    width: 250,
    color: 3e2723,
    },

    addWrapper: {
    width: 60,
    height: 60,
    backgroundColor: "#A1887F",
    borderRadius: 60,
    justifyContent: "center",
    alignItems: "center",
    borderColor: "#3E2723",
    borderWidth: 1,
    color: "#3E2723",
    },
    
    addText: {},
});