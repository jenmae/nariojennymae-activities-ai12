import React from 'react';
import { View, Text, StyleSheet } from "react-native";

const Task = (props) => {

    return (
        <View style={styles.item}>
            <View style = {style.itemLeft}>
                <View style= {styles.square}></View>
                <Text style= {styles.itemText}>{props.text}</Text>
            </View>
            <View style= {styles.circular}></View>
        </View>
    );
};

    const styles = StyleSheet.create({
        item:{
            backgroundColor: "YellowGreen",
            padding: 16,
            borderRadius: 10,
            flexDirection: "row",
            alignItems: "center",
            justifyContent: "space-between",
            marginBottom: 20,
            borderColor: "black",
        },
        itemLeft:{
            flexDirection: "row",
            alignItems: "center",
            flexWrap: "wrap",
        },
        square: {
            width: 25,
            height: 25,
            backgroundColor: "White",
            opacity: 0.4,
            borderRadius: 5,
            marginRight: 16,
        },
        itemText:{
            color: "black",
            maxWidth: "90%",
        },
        circular: {
            width: 15,
            height: 15,
            boderColor: "YellowGreen",
            borderEndWidth: 3,
            borderRadius: 6,
        },
    });
    export default Task;